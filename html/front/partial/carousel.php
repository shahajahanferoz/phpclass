<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
          <!-- <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button> -->
          <?php
          $counter = 0;
          $active = "active";
          foreach ($slides as $key => $slide):
          ?>
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="<?= $counter ?>" class="<?= $active?>" aria-current="true"  aria-label="Slide 2"></button>
          <?php
          $counter++;
          $active = "";
          endforeach;
          ?>
        </div>
        <div class="carousel-inner">
          <?php
          $active = "active";
          foreach ($slides as $key => $slide):
          ?>
          <div class="carousel-item <?= $active ?>">
            <img src="img/<?= $slide['picture']?>" class="d-block w-100" height="500" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h5><?= $slide['captiontitle']?></h5>
              <p><?= $slide['caption']?></p>
            </div>
          </div>
          <?php
          $active = "";
          endforeach;
          ?>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>


    