<?php
namespace Pondit\Sliders;

class Slider{
    public $title = null;

    public $picture = null;

    public $captiontitle = null;

    public $caption = null;

    public function all(){
        return [
            0=>['picture'=>'img1.jpg','captiontitle'=>'first slide label','caption'=>'Some representative placeholder content for the second slide.'], 
            1=>['picture'=>'img2.jpg','captiontitle'=>'Second slide label','caption'=>'Some representative placeholder content for the second slide.'], 
            2=>['picture'=>'img3.jpg','captiontitle'=>'third slide label','caption'=>'Some representative placeholder content for the second slide.'], 
            3=>['picture'=>'img4.jpg','captiontitle'=>'fourth slide label','caption'=>'Some representative placeholder content for the second slide.'], 
          ];
    }
}
?>