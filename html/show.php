<?php
include_once('Person.php');
include_once('Course.php');

$aloy = new Person;
$aloy->name = "Aloy ";
$aloy->sayHello();
$aloy->sayName();

$php = new Course();
$php->title = "web design and development";
echo $aloy->sayName() . "is a student of course " . $php->title;